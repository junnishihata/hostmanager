class CreateBars < ActiveRecord::Migration
  def change
    create_table :bars do |t|
      t.string :name
      t.string :url
      t.integer :good
      t.integer :bad

      t.timestamps null: false
    end
  end
end
