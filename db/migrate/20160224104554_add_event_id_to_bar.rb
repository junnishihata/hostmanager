class AddEventIdToBar < ActiveRecord::Migration
  def change
    add_column :bars, :event_id, :integer
  end
end
