class CreateBarelections < ActiveRecord::Migration
  def change
    create_table :barelections do |t|
      t.integer :event_id
      t.integer :bar_id
      t.integer :invitee_id
      t.boolean :choiced
      t.integer :election_id

      t.timestamps null: false
    end
  end
end
