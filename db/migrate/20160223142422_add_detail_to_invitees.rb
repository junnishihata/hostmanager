class AddDetailToInvitees < ActiveRecord::Migration
  def change
    add_column :invitees, :name, :string
    add_column :invitees, :comment, :text
    add_column :invitees, :atendance, :boolean
  end
end
