class AddFlagToInvitee < ActiveRecord::Migration
  def change
    add_column :invitees, :flag, :boolean
  end
end
