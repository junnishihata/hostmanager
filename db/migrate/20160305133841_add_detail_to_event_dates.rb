class AddDetailToEventDates < ActiveRecord::Migration
  def change
    add_column :event_dates, :date, :date
    add_column :event_dates, :event_id, :integer
    add_column :event_dates, :good, :integer
    add_column :event_dates, :bad, :integer
  end
end
