class Event < ActiveRecord::Base
  has_many :invitees,      dependent: :destroy
  accepts_nested_attributes_for :invitees, :allow_destroy => true
  has_many :bars
  has_many :event_dates, :dependent => :destroy
  accepts_nested_attributes_for :event_dates

  validates :title, presence: true , uniqueness: true
  validates :detail, presence: true


    def self.eventAdd(param,uid)
          event = Event.create(
              title:    param["title"],
              date:     param["date"],
              detail:   param["detail"],
              userid:   uid
            )

    end
    
    def to_param
      return title
    end
end
