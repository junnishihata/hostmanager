class Bar < ActiveRecord::Base
  validates :url, format: /\A#{URI::regexp(%w(http https))}\z/
  belongs_to :event
  def self.barAdd(param,event_id)
        bar = Bar.create(
            name:    param["name"],
            url:     param["url"],
            good:   param["good"],
            bad:      param["bad"],
            event_id: event_id
            #userid:   uid
          )
  end

end
