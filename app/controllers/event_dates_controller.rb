class EventDatesController < ApplicationController
    layout 'application'
    def create
        @event = Event.find(params[:event_id])
        @event_date = @event.event_dates.create(event_date_params)
    end
    private
        def event_date_params
            params[:event_date].permit(:date)
        end
end
