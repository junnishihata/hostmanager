class InviteesController < ApplicationController
  layout 'application'
  def new
    @invitee = Invitee.new
    
  end


  def create
    @event = Event.find(params[:event_id])
    @invitee = @event.invitees.create(invitee_params)
    redirect_to event_path(@event.id)

  end

  private
    def invitee_params
      params[:invitee].permit(:name,:comment,:atendance)
    end
end
