class TopController < ApplicationController
  layout 'toppage'
  def login
    if user_signed_in?
      redirect_to user_root_path
    end
  end
end
