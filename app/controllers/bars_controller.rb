class BarsController < ApplicationController
  layout 'application'
  before_action :set_bar, only:[:show,:edit,:update,:destroy]
  def index
      @bars = Bar.all

      eventid = params[:event_id]
      sql = "SELECT * FROM bars;"
      results = ActiveRecord::Base.connection.execute(sql)

      p results

      @bar_tmp = Array.new

      #ログイン中のユーザのUIDに合致するものだけ投げる
      results.each{ |bar|
        p bar
        p bar["eventid"]
        if(bar["event_id"].to_i == eventid.to_i)
          @bar_tmp.push(bar)
        end
      }
      p @bar_tmp
      @bars = @bar_tmp
  end

  def new
    @bar = Bar.new
  end
  def edit
  end

  def create
    event_id = Event.select('id').find_by(:title => params[:title])
    p event_id
    #event_id = event_id.id

    @bar =  Bar.barAdd(bar_params,event_id)

    if @bar.save
          redirect_to event_path(title: params[:title])
    else
          render 'new'
    end
  end

  def show
    @bar = Bar.find(params[:id])

  end

  def update
      if @bar.update(bar_params)
          redirect_to event_bars_path
      else
          render 'edit'
      end
  end



  private
    def bar_params
        params[:bar].permit(:name,:url,:good,:bad,:eventid)
    end
    def set_bar
      @bar = Bar.find(params[:id])
    end
end
