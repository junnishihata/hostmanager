class EventsController < ApplicationController
    layout 'application'
    before_action :set_event, only:[:show,:edit,:update,:destroy]
    def index
        @events = Event.where(userid: current_user.uid)

=begin
        @event_tmp = Array.new

        #ログイン中のユーザのUIDに合致するものだけ投げる
        @events.each{ |event|
          if(event["userid"] == current_user.uid)
            @event_tmp.push(event)
          end
        }
        @events = @event_tmp
        
=end
    end

    def new
        @event = Event.new
        event_date = EventDate.new
        @event.event_dates << event_date
        #@event.event_dates.build
    end

    def create
        #@event = Event.new(event_params)

        #@event =  Event.eventAdd(event_params,current_user.uid)

        event_params["userid"] = current_user.uid
        @event = Event.create(event_params)

        if @event.save
          redirect_to events_path
        else
          p "errors",@event.errors
          render :action => :new
        end
    end


    def show
      @event = Event.find_by(:title => params[:title])
      
      event_id = Event.select('id').find_by(:title => params[:title])
      puts event_id.id
      @invitee = Invitee.find_by(:event_id => event_id.id)
      
      puts @event.is_a?(Object)
      p @event
      puts @invitee.is_a?(Object)
      p @invitee
    end
    def edit
    end

    def update
        if @event.update(event_params)
            redirect_to event_path
        else
            render 'edit'
        end
    end


    private
        def event_params
            params[:event].permit(:title,:date,:detail,:userid,
            invitees_attributes:[:id,:name],
            event_dates_attributes:[:id,:event_id,:date,:good,:bad,:_destroy]
            )
        end


        def set_event
          @event = Event.find_by(title: params[:title])
        end
end
